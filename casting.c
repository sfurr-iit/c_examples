/* 
 * Casting Example
 */

#include<stdio.h>
#include<stdlib.h>

int main(){
  unsigned int x = 0xdeadbeef;
  unsigned char y = (char)x;

  printf("x: 0x%x, y: 0x%x.\n", x, y);
}
