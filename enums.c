/* 
 * Enum example
 */

#include<stdio.h>

enum city {Chicago, Paris, Milan};

int main(){
  enum city x = Chicago;
  printf("x is Milan: %d.\n", x==Milan);
  printf("x is Chicago: %d.\n", x==Chicago);
  return 0;
}
