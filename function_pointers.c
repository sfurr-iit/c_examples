/* 
 * Function Pointers Example
 */

#include<stdio.h>

int adder (int x, int y){
  return x + y;
}

int main (){
  int (*p) (int, int) = &adder;
  int result = (*p)(3, 5);
  printf ("Result is: %d.\n", result);
}
