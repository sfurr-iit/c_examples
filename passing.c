/* 
 * Pass by Value vs Pass by Reference Example 
 */

#include<stdio.h>

void set_x_value (int x, int set);
void set_x_reference (int *x, int set);

int main(){
  int x = 10;
  printf("Initially x is: %d.\n", x);

  // Note the difference in output.
  set_x_value (x, 5);
  printf("x is now: %d.\n", x);
  set_x_reference (&x, 5);
  printf("x is now: %d.\n", x);
  
  return 0;
}

/*
 * Passes x by value
 */
void set_x_value (int x, int set){
  x = set;
}

/*
 * Passes x by reference
 */
void set_x_reference (int *x, int set){
  *x = set;
}
