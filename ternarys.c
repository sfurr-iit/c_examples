/* 
 * Ternary operators example
 */

#include<stdio.h>

int main(){
  int x = 10;
  printf("x is: %d.\n", x);
  
  if (x==10){
    x = 5;
  }
  else{
    x = 9;
  }
  printf("x is: %d.\n", x);
  
  x = (x==10 ? 5 : 9);
  printf("x is: %d.\n", x);
}
