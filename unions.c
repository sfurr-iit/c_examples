/* 
 * Union example
 */

#include<stdio.h>

typedef union{
  struct {
    unsigned char first;
    unsigned char second;
    unsigned char third;
    unsigned char fourth;
  } bytes;
  unsigned int word;
} mem_word;

int main(){
  mem_word my_word;
  my_word.word = 0xdeadbeef;
  printf ("Our word is now: 0x%x.\n", my_word.word);
  printf ("The third byte of our word is now: 0x%x.\n", my_word.bytes.third);
  my_word.bytes.third = 0x7;
  printf ("Our word is now: 0x%x.\n", my_word.word);
  printf ("The third byte of our word is now: 0x%x.\n", my_word.bytes.third);  
  return 0;
}
